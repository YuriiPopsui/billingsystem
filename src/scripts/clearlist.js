const forPayment = document.querySelector('.form__summary-list');
const rightCheckbox = document.querySelectorAll('input[type="checkbox"]');

function clearList(list) {
  list.forEach(item => {item.remove();});
  forPayment.insertAdjacentHTML('beforeend', `<li class="list__item list__total">
  <p><span class="list__item-label">Всего</span>
    <span class="price" id="price">$ <b class = "total-summ">0</b></span>
  </p>
</li>`);

rightCheckbox.forEach(item => {
  if(item.getAttribute('checked') === '') {
    item.removeAttribute('checked');
  } 
});
}

export default clearList;