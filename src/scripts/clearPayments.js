import clearList from "./clearlist";

const clearBtn = document.querySelector('.btn-outline');
function clearButton(array) {
  clearBtn.addEventListener('click', () => {
  array.length = 0;
  const listPayment = document.querySelectorAll('.list__item');
  clearList(listPayment);
});
}

export default clearButton;
