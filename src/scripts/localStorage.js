import { readyToPay } from "./payment";
import costCalculation from './saveBtn';
import clearList from './clearlist';

function saveData(payments) {
  let total = 0;
  if(JSON.parse(localStorage.getItem('readyToPay')) !== null) {
      const readyToPay1 = JSON.parse(localStorage.getItem('readyToPay'));
      readyToPay1.forEach(payment => {
        readyToPay.push(payment);
        document.querySelector(`input[data-id="${payment.id}"]`).setAttribute('checked', '');
      total += payment.totalCost;
      document.querySelector('.form__summary-list').insertAdjacentHTML('afterbegin', `<li class="list__item">
      <p><span class="list__item-label">${document.getElementById('meters').querySelector(`option[value="${payment.meterId}"]`).innerText}</span>
      <span class="price">$ <b>${payment.totalCost}</b></span>
      </p>
      </li>`);
    });
      document.querySelector('#price').innerHTML = `$ <b class = "total-summ">${total}</b>`;  
  } else {
    return;
  }
}

export default saveData;