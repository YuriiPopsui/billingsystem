import { tarifs } from "./constants";
import savePayment from "./payment";
import { readyToPay } from "./payment";
const saveButton  = document.querySelector('.btn');
const forPayment = document.querySelector('.form__summary-list');
let greatTotal = 0;

function costCalculation(payment, payments) {
  const total = document.querySelector('#price');
  saveButton.addEventListener('click', (e) => {
    e.preventDefault();

    let totalCost = Math.floor((payment.current - payment.previous) * tarifs[`${payment.id}`] *10000) / 10000;
    payment.totalCost = totalCost;
    payments.push(Object.defineProperties({}, Object.getOwnPropertyDescriptors(payment)));
    // console.log(payment);
    document.querySelector('#previous').value = '';
    document.querySelector('#current').value = '';
    forPayment.insertAdjacentHTML('afterbegin', `<li class="list__item">
    <p><span class="list__item-label">${document.getElementById('meters').querySelector(`option[value="${payment.meterId}"]`).innerText}</span>
      <span class="price">$ <b>${totalCost}</b></span>
    </p>
  </li>`);
    greatTotal += totalCost;
    total.innerHTML = `$ <b class = "total-summ">${greatTotal}</b>`;
    savePayment(payments, payment);
    for(let item in payment) {
      delete payment[item];
    } 
  });
}

export default costCalculation;

// let greatB = payments.reduce((accum, curr) => {accum.totalCost + curr.totalCost;
    //   console.log(accum.totalCost);
    //   console.log(curr.totalCost);
    // });
    // console.log('-------------');
    // console.log(greatB);
    // console.log(greatB.totalCost);