import transactionsStatus from "./transactions";
const paymentBtn = document.querySelector('.btn-to-payment');
export const readyToPay = [];

function savePayment(payments, payment) {
  payments.forEach(item => { 
    if(item.id === payment.id) {
      document.querySelector(`input[data-id="${payment.id}"]`).setAttribute('checked', '');
      readyToPay.push(item);
    }
  });
  localStorage.setItem('readyToPay', JSON.stringify(readyToPay));
}
  paymentBtn.addEventListener('click', (e) => {
    e.preventDefault();
    transactionsStatus();
    readyToPay.forEach(item => {
      console.log(`${item.id} paid`);
    });
    document.querySelectorAll('input[type="checkbox"]').forEach(item => {
      if(item.getAttribute('checked') === '') {
        item.removeAttribute('checked');
      } 
    });
  });

export default savePayment;