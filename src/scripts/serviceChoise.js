import { descriptors } from './descriptors';

const companies = document.querySelectorAll('.left__company');
const paymentTitle = document.querySelector('.center__title');
const paymentDescript = document.querySelector('.center__desc');
for(let item of companies) {
  item.classList.remove('selected');
}
function serviceChoise(payment) {
  companies.forEach(item => {
    item.addEventListener('click', () => {
     for(let item of companies) {
       item.classList.remove('selected');
     }
    item.classList.add('selected');
    payment.id = item.dataset.id; 
    paymentTitle.innerText = item.querySelector('p').innerText;
    paymentDescript.innerText = descriptors[item.dataset.id];
    // console.log(item);
    // console.log('payment is ', payment);
    });
  });
}

export default serviceChoise;

