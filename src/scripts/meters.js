const meter = document.getElementById('meters');
const dataPrevious = document.querySelector('#previous');
const dataCurrent = document.querySelector('#current');

function findOption(payment) {
  payment.meterId = meter.value;
  meter.addEventListener('change', () => {
    payment.meterId = meter.value;
    // console.log('payment is ', payment);
  });
  dataPrevious.addEventListener('change', () => {
    if(numberValidation(dataPrevious.value)) {
      payment.previous = dataPrevious.value;
    } else {
      alert ('Заполните форму правильно');
    }
    // console.log(payment);
  });
  dataCurrent.addEventListener('change', () => {
    if(numberValidation(dataCurrent.value) && valueValidation()) {
      payment.current = dataCurrent.value;
    } else {
      alert ('Заполните форму правильно');
    }
    // console.log(payment);
  });

}
function numberValidation(number) {
  return Math.sign(number) === 1 ? true : false;
}
function valueValidation() {
  return dataCurrent.value > dataPrevious.value ? true : false;
}

export default findOption;