import { readyToPay } from "./payment";
import { balance } from "./index";
import { translates } from "./descriptors";
import clearList from './clearlist';
const transactionsList = document.querySelector('.transactions__list');

function transactionsStatus() {
  let newBalance = balance;
  readyToPay.forEach(item => {
    newBalance -= item.totalCost;
    if(newBalance >= item.totalCost) {
      transactionsList.insertAdjacentHTML('afterbegin', `<li class=" list__item-right">${translates[item.id]}: успешно оплачено</li>`);
    } else {
      transactionsList.insertAdjacentHTML('beforeend', `<li class=" list__item-right list__item-error">${translates[item.id]}: Ошибка транзакции</li>`);
    }
  });
  if (newBalance >= 0) {
    localStorage.clear();
    clearList(document.querySelectorAll('.list__item'));
  }
}
export default transactionsStatus;