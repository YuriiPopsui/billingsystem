import '../styles/index.scss';
import { tarifs } from './constants';
import serviceChoise from './serviceChoise';
import findOption from './meters';
import clearList from "./clearlist";
import costCalculation from './saveBtn';
import clearButton from './clearPayments';
import saveData from './localStorage';

export const listPayment = document.querySelectorAll('.list__item');
export let balance = 1000;
const payments = [];
const payment = {};
clearList(listPayment);
saveData(payments);
serviceChoise(payment);
findOption(payment);
costCalculation(payment, payments);
clearButton(payments, listPayment);

