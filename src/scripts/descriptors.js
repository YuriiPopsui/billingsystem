export const descriptors = {
  taxes: 'Оплата налогов',
  water: 'Оплата холодного водоснабжения',
  internet: 'Оплата интернета',
  security: 'Оплата охранных услуг',
  exchange: 'Обмен валют'
};
export const translates = {
  taxes: 'Налоги',
  water: 'Холодная вода',
  internet: 'Интернет',
  security: 'Охрана',
  exchange: 'Обмен валют'
};

